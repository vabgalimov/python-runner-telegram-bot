Telegram bot which run python code.

Run bot:

1. Create env file with bot's token
TOKEN={token}

2. Install requirement modules
pip install -r req.txt

3. Run pybot.py
python pybot.py


Commands:

/start, /help - Help text of bot
