from dotenv import dotenv_values
from subprocess import run as run_process, PIPE, TimeoutExpired
from telebot import TeleBot, apihelper
from os import remove

TOKEN = dotenv_values(".env")["TOKEN"]
LIMIT_SECONDS = 30

bot = TeleBot(TOKEN, parse_mode=None)
bot_id = bot.get_me().id

@bot.message_handler(commands=["start", "help"])
def help(msg):
    help_text = """Python 3 runner.
Using: reply python code to this bot."""
    bot.reply_to(msg, help_text)

val = -1
def val_next():
    global val
    val = (val + 1) % 10
    return val

def run_pycode(code, run_count):
    path = f".run_{run_count}.py"
    with open(path, "w+") as f:
        f.write(code)
    com = ['python', path]
    try:
      res = run_process(com, stdout=PIPE, stderr=PIPE, timeout=LIMIT_SECONDS)
    except TimeoutExpired:
      return None
    finally:
      remove(path)
    out = res.stdout
    err = res.stderr
    return (err if res.returncode else out).decode("utf-8")

def check_msg(msg):
    if msg.content_type != "text":
        return False
    if msg.chat.type == "private":
        return True
    reply = msg.reply_to_message
    return reply and reply.from_user.id == bot_id
    
@bot.message_handler(func=check_msg)
def get_msg(msg):
    run_msg = bot.reply_to(msg, "Running...")
    result = run_pycode(msg.text, val_next())
    apihelper.delete_message(TOKEN, msg.chat.id, run_msg.id)
    if result == None:
        bot.reply_to(msg, "<process was killed>")
        return
    try:
        bot.reply_to(msg, result.strip() or "<empty>")
    except Exception as e:
        bot.reply_to(msg, f"Telegram error:\n{e}")

bot.delete_webhook()
print("Bot started...")
bot.infinity_polling()
